package test.insoftar.com.app.general;

public class ApiResponse {
	private boolean status;
	private String message;
	private String messageType;
	private String statusCode;
	private Integer responseId;
	
	public ApiResponse() {
		
	}
	public ApiResponse(boolean status, String message, String messageType, String statusCode, Integer responseId) {
		super();
		this.status = status;
		this.message = message;
		this.messageType = messageType;
		this.statusCode = statusCode;
		this.responseId = responseId;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getMessageType() {
		return messageType;
	}
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public Integer getResponseId() {
		return responseId;
	}
	public void setResponseId(Integer responseId) {
		this.responseId = responseId;
	}
}

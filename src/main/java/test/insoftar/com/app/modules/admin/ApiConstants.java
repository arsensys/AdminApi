package test.insoftar.com.app.modules.admin;

public class ApiConstants {
	
	public static final String API_RESPONSE_STATUS_CODE_SUCCESS = "1";
	public static final String API_RESPONSE_STATUS_CODE_ERROR = "0";
	public static final String API_RESPONSE_STATUS_CODE_WARNING = "2";
	public static final String API_RESPONSE_STATUS_CODE_INFO = "3";

}

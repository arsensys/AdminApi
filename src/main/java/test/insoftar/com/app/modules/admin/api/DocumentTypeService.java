package test.insoftar.com.app.modules.admin.api;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import test.insoftar.com.app.modules.admin.dao.IDocumentTypeDao;
import test.insoftar.com.app.modules.admin.dao.IUserDao;
import test.insoftar.com.app.modules.admin.dto.DocumentTypeDto;
import test.insoftar.com.app.modules.admin.dto.UserDto;
import test.insoftar.com.app.modules.admin.entities.DocumentTypeEntity;
import test.insoftar.com.app.modules.admin.entities.UserEntity;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping(value="api/documentTypes")
public class DocumentTypeService {

	@Autowired
	IDocumentTypeDao documentTypeDao;
	
	@GetMapping("/list")
	public List<DocumentTypeDto> listUsers() {
		List<DocumentTypeDto> documentTypes = new ArrayList<DocumentTypeDto>();
		
		try {
			for (DocumentTypeEntity documentType : documentTypeDao.findAll()) {
				documentTypes.add(new DocumentTypeDto(
						documentType.getId(), 
						documentType.getAbbreviation(), 
						documentType.getDescription()
						));
				
			}
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return documentTypes;
	}
}

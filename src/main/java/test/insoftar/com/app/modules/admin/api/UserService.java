package test.insoftar.com.app.modules.admin.api;

import java.util.ArrayList;
import java.util.List;

import org.apache.catalina.authenticator.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import test.insoftar.com.app.general.ApiResponse;
import test.insoftar.com.app.modules.admin.ApiConstants;
import test.insoftar.com.app.modules.admin.dao.IUserDao;
import test.insoftar.com.app.modules.admin.dto.UserDto;
import test.insoftar.com.app.modules.admin.entities.UserEntity;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping(value = "api/users")
public class UserService {

	@Autowired
	IUserDao userDao;

	@GetMapping("/list")
	public List<UserDto> listUsers() {
		List<UserDto> users = new ArrayList<UserDto>();

		try {
			for (UserEntity user : userDao.findAll()) {
				users.add(new UserDto(user.getId(), user.getDocumentTypeId(), user.getDocument(), user.getNames(),
						user.getLastNames(), user.getEmail(), user.getPhone(), user.getStatus()));

			}

		} catch (Exception e) {
			// TODO: handle exception
		}

		return users;
	}

	@GetMapping("/getUser")
	public UserDto getUser(Integer userId) {
		UserDto user = null;
		try {
			if (userId != null) {
				UserEntity userEntity = userDao.findById(userId);
				if (userEntity != null) {

					user = new UserDto(userEntity.getId(), userEntity.getDocumentTypeId(), userEntity.getDocument(),
							userEntity.getNames(), userEntity.getLastNames(), userEntity.getEmail(),
							userEntity.getPhone(), userEntity.getStatus());
				}
			}

		} catch (Exception e) {
		}

		return user;
	}

	@PostMapping("/create")
	public ApiResponse createUser(@RequestBody UserDto user) {
		ApiResponse apiResponse = new ApiResponse(false, "Undefined", "warning",
				ApiConstants.API_RESPONSE_STATUS_CODE_WARNING, 0);

		try {
			if (userDao.findByEmail(user.getEmail()) == null) {
				if (userDao.findByDocument(user.getDocumentTypeId(), user.getDocument()) == null) {
					UserEntity userEntity = new UserEntity();
					// userEntity.setId(0);
					userEntity.setDocumentTypeId(user.getDocumentTypeId());
					userEntity.setDocument(user.getDocument());
					userEntity.setNames(user.getNames());
					userEntity.setLastNames(user.getLastNames());
					userEntity.setEmail(user.getEmail());
					userEntity.setPhone(user.getPhone());
					userEntity.setStatus(user.getStatus());
					userDao.save(userEntity);

					apiResponse.setStatus(true);
					apiResponse.setMessage("Usuario creado correctamente.");
					apiResponse.setMessageType("successful");
					apiResponse.setStatusCode(ApiConstants.API_RESPONSE_STATUS_CODE_SUCCESS);
				} else {
					apiResponse.setMessage("Ya existe usuario con este Tipo y Número de Documento.");
				}
			} else {
				apiResponse.setMessage("Ya existe usuario con este Email.");
			}
		} catch (Exception e) {
			apiResponse.setMessage("Error al intentar crear Usuario");
			apiResponse.setMessageType("error");
			apiResponse.setStatusCode(ApiConstants.API_RESPONSE_STATUS_CODE_ERROR);
			System.out.println("UserService -> createUser -> Exception: " + e.getCause() + e.getMessage());
		}

		return apiResponse;
	}

	@PostMapping("/update")
	public ApiResponse updateUser(@RequestBody UserDto user) {
		ApiResponse apiResponse = new ApiResponse(false, "Undefined", "warning",
				ApiConstants.API_RESPONSE_STATUS_CODE_WARNING, 0);

		try {

			if (user != null && user.getId() != null) {
				UserEntity userEntity = userDao.findById(user.getId());
				if (userEntity != null) {
					
					/*Verificar existencia de ussuario por email*/
					UserEntity userByEmail = userDao.findByEmail(user.getEmail());
					if ( userByEmail == null || userByEmail.getId() == userEntity.getId()) {
						
						/*Verificar existencia de ussuario por Documento*/
						UserEntity userByDocument = userDao.findByDocument(user.getDocumentTypeId(), user.getDocument());
						if (userByDocument == null || userByDocument.getId() == userEntity.getId()) {
							userEntity.setDocumentTypeId(user.getDocumentTypeId());
							userEntity.setDocument(user.getDocument());
							userEntity.setNames(user.getNames());
							userEntity.setLastNames(user.getLastNames());
							userEntity.setEmail(user.getEmail());
							userEntity.setPhone(user.getPhone());
							userEntity.setStatus(user.getStatus());
							userDao.save(userEntity);
							

							apiResponse.setStatus(true);
							apiResponse.setMessage("Usuario actualizado correctamente.");
							apiResponse.setMessageType("successful");
							apiResponse.setStatusCode(ApiConstants.API_RESPONSE_STATUS_CODE_SUCCESS);
						} else {
							apiResponse.setMessage("Ya existe usuario con este Tipo y Número de Documento.");
						}
					} else {
						apiResponse.setMessage("Ya existe usuario con este Email.");
					}

				} else {
					apiResponse.setMessage("Usuario no existe.");
				}
			} else {
				apiResponse.setMessage("Parametros incorrectos.");
			}
		} catch (Exception e) {
			apiResponse.setMessage("Error al intentar actualizar el Usuario");
			apiResponse.setMessageType("error");
			apiResponse.setStatusCode(ApiConstants.API_RESPONSE_STATUS_CODE_ERROR);
			System.out.println("UserService -> createUser -> Exception: " + e.getCause() + e.getMessage());
		}

		return apiResponse;
	}
}

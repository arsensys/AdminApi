package test.insoftar.com.app.modules.admin.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import test.insoftar.com.app.modules.admin.entities.DocumentTypeEntity;
import test.insoftar.com.app.modules.admin.entities.UserEntity;

@Repository
public class DocumentTypeImp implements IDocumentTypeDao {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public DocumentTypeEntity findById(Integer userId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<DocumentTypeEntity> findAll() {
		return entityManager.createQuery(" from DocumentTypeEntity").getResultList();
	}

	@Override
	public void save(DocumentTypeEntity user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(DocumentTypeEntity user) {
		// TODO Auto-generated method stub
		
	}
	

}

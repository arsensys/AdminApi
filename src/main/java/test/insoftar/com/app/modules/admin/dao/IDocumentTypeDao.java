package test.insoftar.com.app.modules.admin.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import test.insoftar.com.app.modules.admin.entities.DocumentTypeEntity;
import test.insoftar.com.app.modules.admin.entities.UserEntity;

public interface IDocumentTypeDao {

	public DocumentTypeEntity findById(Integer userId);
	public List<DocumentTypeEntity> findAll();
	public void save(DocumentTypeEntity user);
	public void update(DocumentTypeEntity user);

}

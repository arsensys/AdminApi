package test.insoftar.com.app.modules.admin.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import test.insoftar.com.app.modules.admin.entities.UserEntity;

public interface IUserDao {

	public UserEntity findById(Integer userId);
	public UserEntity findByEmail(String email);
	public UserEntity findByDocument(Integer documentTypeId, String document);
	public List<UserEntity> findAll();
	public void save(UserEntity user);
	public void update(UserEntity user);

}

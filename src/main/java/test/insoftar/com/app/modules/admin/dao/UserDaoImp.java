package test.insoftar.com.app.modules.admin.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import test.insoftar.com.app.modules.admin.entities.UserEntity;

@Repository
public class UserDaoImp implements IUserDao {

	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public UserEntity findById(Integer userId) {
		UserEntity user = null;		
		try {
			user = entityManager.find(UserEntity.class, userId);
			}catch (Exception e) {
				System.out.println(e.getMessage());
			}finally {
				return user;	
			}
	}
	
	@Override
	public UserEntity findByEmail(String email) {
		String sql = "select u from UserEntity u where u.email = :email";
		UserEntity user = null;
		
		try {
		user = entityManager.createQuery(sql, UserEntity.class).setParameter("email", email).getSingleResult();
		}catch (Exception e) {
			System.out.println(e.getMessage()+"ee");
		}finally {
			return user;	
		}	
	}



	@Override
	public UserEntity findByDocument(Integer documentTypeId, String document) {
		String sql = "select u from UserEntity u where u.documentTypeId = :documentTypeId and u.document = :document";
		UserEntity user = null;
		
		try {
		user = entityManager.createQuery(sql, UserEntity.class)
				.setParameter("documentTypeId", documentTypeId)
				.setParameter("document", document).getSingleResult();
		}catch (Exception e) {
			System.out.println(e.getMessage());
		}finally {
			return user;	
		}	
	}

	@Override
	@Transactional(readOnly = true)
	public List<UserEntity> findAll() {
		return entityManager.createQuery(" from UserEntity").getResultList();
	}
	

	@Override
	@Transactional
	public void save(UserEntity user) {
		entityManager.persist(user);
	}

	@Override
	public void update(UserEntity user) {
		// TODO Auto-generated method stub
		
	}
}

package test.insoftar.com.app.modules.admin.dto;

public class DocumentTypeDto {
	private Integer id;

	private String abbreviation;
	private String description;

	
	public DocumentTypeDto() {

	}


	public DocumentTypeDto(Integer id, String abbreviation, String description) {
		super();
		this.id = id;
		this.abbreviation = abbreviation;
		this.description = description;
	}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getAbbreviation() {
		return abbreviation;
	}


	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}
}

package test.insoftar.com.app.modules.admin.dto;

public class UserDto {
	private Integer Id;
	private Integer documentTypeId;
	private String document;
	private String names;
	private String lastNames;
	private String email;
	private String phone;
	private String status;
	
	public UserDto() {

	}

	public UserDto(Integer id, Integer documentTypeId, String document, String names, String lastNames, String email,
			String phone, String status) {
		super();
		Id = id;
		this.documentTypeId = documentTypeId;
		this.document = document;
		this.names = names;
		this.lastNames = lastNames;
		this.email = email;
		this.phone = phone;
		this.status = status;
	}

	public Integer getId() {
		return Id;
	}

	public void setId(Integer id) {
		Id = id;
	}

	public Integer getDocumentTypeId() {
		return documentTypeId;
	}

	public void setDocumentTypeId(Integer documentTypeId) {
		this.documentTypeId = documentTypeId;
	}

	public String getDocument() {
		return document;
	}

	public void setDocument(String document) {
		this.document = document;
	}

	public String getNames() {
		return names;
	}

	public void setNames(String names) {
		this.names = names;
	}

	public String getLastNames() {
		return lastNames;
	}

	public void setLastNames(String lastNames) {
		this.lastNames = lastNames;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}

package test.insoftar.com.app.modules.admin.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

@Entity
@Table(name="gen_tipos_documento")
public class DocumentTypeEntity {


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="tip_conse")
	@NotEmpty
	private Integer id;
	
	@Column(name="tip_abrev")
	@NotEmpty(message = "La abreviación es requerida.")
	private String abbreviation;
	
	@Column(name="tip_descr")
	@NotEmpty(message = "La Descripción es requerida.")
	private String description;

	
	public DocumentTypeEntity() {

	}


	public DocumentTypeEntity(Integer id, String abbreviation, String description) {
		super();
		this.id = id;
		this.abbreviation = abbreviation;
		this.description = description;
	}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getAbbreviation() {
		return abbreviation;
	}


	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}
	
	
}

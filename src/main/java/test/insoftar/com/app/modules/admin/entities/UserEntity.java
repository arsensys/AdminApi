package test.insoftar.com.app.modules.admin.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Entity
@Table(name="adm_usuarios")
public class UserEntity {


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="usu_conse")
	private Integer Id;
	
	@Column(name="tip_conse")
	@NotNull(message = "El tipo documento es requerido.")
	private Integer documentTypeId;
	
	@Column(name="usu_docum")
	@Pattern(regexp = "[0-9]+", message = "El documento debe ser numerico")
	private String document;
	
	@Column(name="usu_nombr")
	@NotEmpty(message = "El nombre es requerido")
	private String names;
	
	@Column(name="usu_apell")
	@NotEmpty(message = "El Apellido es requerido.")
	private String lastNames;
	
	@Column(name="usu_email", unique = true)
	@Email(message = "El email no tiene un formato correcto!")
	private String email;
	
	@Column(name="usu_telef")
	@Pattern(regexp = "[0-9]+", message = "El Telefono debe ser numerico")
	private String phone;
	
	@Column(name="usu_estad")
	@NotEmpty(message = "El estado no es correcto.")
	private String status;

	public Integer getId() {
		return Id;
	}

	public void setId(Integer id) {
		Id = id;
	}

	public Integer getDocumentTypeId() {
		return documentTypeId;
	}

	public void setDocumentTypeId(Integer documentTypeId) {
		this.documentTypeId = documentTypeId;
	}

	public String getDocument() {
		return document;
	}

	public void setDocument(String document) {
		this.document = document;
	}

	public String getNames() {
		return names;
	}

	public void setNames(String names) {
		this.names = names;
	}

	public String getLastNames() {
		return lastNames;
	}

	public void setLastNames(String lastNames) {
		this.lastNames = lastNames;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
